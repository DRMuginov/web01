package com.example.web01

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.support.SpringBootServletInitializer

@SpringBootApplication
class WebApplication : SpringBootServletInitializer() {
    override  fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(WebApplication::class.java)
    }

    companion object {

        @Throws(Exception::class)
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(WebApplication::class.java, *args)
        }
    }
}


