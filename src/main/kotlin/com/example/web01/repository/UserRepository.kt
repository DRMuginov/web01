package com.example.web01.repository

import com.example.web01.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.*

interface UserRepository : JpaRepository<User, Long> {
    fun findByUsername(username: String): User
}