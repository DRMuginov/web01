package com.example.web01.service

import com.example.web01.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.util.HashSet

@Service
class UserDetailsServiceImpl : UserDetailsService {
    @Autowired
    private val userRepository: UserRepository? = null

    @Transactional(readOnly = true)
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository!!.findByUsername(username)

        val grantedAuthorities = HashSet<GrantedAuthority>()
        for (role in user.roles) {
            grantedAuthorities.add(SimpleGrantedAuthority(role.name))
        }

        return org.springframework.security.core.userdetails.User(user.username, user.password, grantedAuthorities)
    }
}
