package com.example.web01.service

import com.example.web01.model.User
import com.example.web01.repository.RoleRepository
import com.example.web01.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

import java.util.HashSet

@Service
class UserServiceImpl : UserService {
    @Autowired
    private val userRepository: UserRepository? = null
    @Autowired
    private val roleRepository: RoleRepository? = null
    @Autowired
    private val bCryptPasswordEncoder: BCryptPasswordEncoder? = null

    override fun save(user: User) {
        user.password = bCryptPasswordEncoder!!.encode(user.password)
        user.roles = HashSet(roleRepository!!.findAll())
        userRepository!!.save(user)
    }

    override fun findByUsername(username: String): User {
        return userRepository!!.findByUsername(username)
    }
}