package com.example.web01.service

interface SecurityService {
    fun findLoggedInUsername(): String

    fun autologin(username: String, password: String)
}