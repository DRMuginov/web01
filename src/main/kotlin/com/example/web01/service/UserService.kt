package com.example.web01.service


import com.example.web01.model.User

interface UserService {
    fun save(user: User)

    fun findByUsername(username: String): User
}
