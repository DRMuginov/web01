package com.example.web01.model

import javax.persistence.*

@Entity
@Table(name = "role")
data class Role(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long = 0L,

    var name: String = "",

    @ManyToMany(mappedBy = "roles")
    var users: Set<User> = setOf()
)