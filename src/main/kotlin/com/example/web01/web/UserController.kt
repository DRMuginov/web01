package com.example.web01.web

import com.example.web01.model.User
import com.example.web01.repository.UserRepository
import com.example.web01.service.SecurityService
import com.example.web01.service.UserDetailsServiceImpl
import com.example.web01.service.UserService
import com.example.web01.service.UserServiceImpl
import org.springframework.security.core.userdetails.UserDetails
import com.example.web01.validator.UserValidator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.security.core.session.SessionRegistry



@Controller
class UserController {
    @Autowired
    private var userService: UserService? = null

    @Autowired
    private var securityService: SecurityService? = null

    @Autowired
    private var userValidator: UserValidator? = null

    @Autowired
    private val userRepository: UserRepository? = null

    @Autowired
    private val sessionRegistry: SessionRegistry? = null

    fun listLoggedInUsers(): List<User> {
        val allPrincipals = sessionRegistry!!.allPrincipals
        val userList: MutableList<User> = mutableListOf()
        for (principal in allPrincipals) {
            if (principal is org.springframework.security.core.userdetails.User) {

                val activeUserSessions = sessionRegistry.getAllSessions(principal, false)

                if (!activeUserSessions.isEmpty()) {
                    userList.add(userRepository!!.findByUsername(principal.username))
                }
            }
        }
        return  userList.sortedBy{ it.id }
    }

    @RequestMapping(value = "/registration", method = [RequestMethod.GET])
    fun registration(model: Model): String {
        model.addAttribute("userForm", User())

        return "registration"
    }

    @RequestMapping(value = "/registration", method = [RequestMethod.POST])
    fun registration(@ModelAttribute("userForm") userForm: User, bindingResult: BindingResult, model: Model): String {
        userValidator!!.validate(userForm, bindingResult)

        if (bindingResult.hasErrors()) {
            return "registration"
        }

        userService!!.save(userForm)

        securityService!!.autologin(userForm.username, userForm.passwordConfirm)

        return "redirect:/welcome"
    }

    @RequestMapping(value = "/login", method = [RequestMethod.GET])
    fun login(model: Model, error: String?, logout: String?): String {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.")

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.")

        return "login"
    }

    @RequestMapping(value = *["/", "/welcome"], method = [RequestMethod.GET])
    fun welcome(model: Model): String {
        model.addAttribute("LoggedUsers", listLoggedInUsers());
        return "welcome"
    }

}